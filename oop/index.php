<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    
    <?php
    include_once 'animal.php';
    include_once 'frog.php';
    include_once 'ape.php';
    $sheep = new Animal("shaun");

    echo $sheep->name; // "shaun"
    echo "</br>";
    echo $sheep->legs; // 4
    echo "</br>";
    echo $sheep->cold_blooded; // "no"
    echo "</br>";

    //! KERA
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"
    echo $sungokong->legs;
    echo "</br>";
    //! KODOK
    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"
    echo $kodok->legs;
    echo "</br>";
    ?>
</body>

</html>