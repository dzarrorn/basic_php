<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping</h1>

    <?php 
        //! NOMOR 13A
        function pagar_bintang($input){
            for ($i=1; $i <= $input ; $i++) { 
                for ($j=1; $j <= $input; $j++) { 
                    if ($i%2==0) {
                        echo("*");
                    }else if($i%2!==0){
                        echo("#");
                    }
                }
                echo("</br>");
                # code...
            }
        }
        echo pagar_bintang(5);
        echo("</br>");
        echo("</br>");
        echo pagar_bintang(8);
        echo("</br>");
        echo("</br>");
        echo pagar_bintang(10);

        // //! NOMOR 14B
        function xo($str){
            $a=substr_count($str,"x");
            $b=substr_count($str,"o");
            echo $a==$b?"benar</br>":"salah</br>";
        }
        echo xo('xoxoxo'); // "Benar"
        echo xo('oxooxo'); // "Salah"
        echo xo('oxo'); // "Salah"
        echo xo('xxooox'); // "Benar"
        echo xo('xoxooxxo'); // "Benar"
        

    ?>

</body>
</html>